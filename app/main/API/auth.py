from flask import Blueprint, request, redirect, url_for, jsonify, make_response
from werkzeug.security import generate_password_hash, check_password_hash
from functools import wraps
from datetime import datetime, timedelta
import jwt
import uuid

from app.main.model.User_model import User
from app import db


bp = Blueprint('auth', __name__)


@bp.route('/signup', methods=['POST', ])
def signup():
    data = request.get_json()

    user = User.query.filter_by(name=data['name']).first()

    if user:
        return make_response('user already exists', 403)

    new_user = User(name=data['name'], password=generate_password_hash(data['password']),
                    public_id=str(uuid.uuid4()))

    db.session.add(new_user)
    db.session.commit()

    return {"message": "Signup success"}


@bp.route('/login', methods=['POST', ])
def login():
    auth = request.get_json()

    if not auth or not auth['name'] or not auth['password']:
        return make_response('could not verify', 403, {'Authentication': 'Login required'})

    user = User.query.filter_by(name=auth['name']).first()

    if user is None:
        return make_response('username incorrect', 403, {'Authentication': 'Login required'})

    if check_password_hash(user.password, auth['password']):
        token = jwt.encode(
            {'public_id': user.public_id, 'exp': datetime.utcnow() + timedelta(seconds=30)},
            "minh", "HS256"
        )
        return jsonify({'token': token, 'username': user.name})
    return make_response('password incorrect', 403, {'Authentication': 'Login required'})


@bp.route('/logout')
def logout():
    return redirect(url_for('index'))


def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = None
        if 'Authorization' in request.headers:
            token = request.headers['Authorization']

        if not token:
            return make_response('need token', 401, {'Authentication': 'Login required'})
        try:
            data = jwt.decode(token, "minh", algorithms=["HS256"])
            current_user = User.query.filter_by(public_id=data['public_id']).first()
        except:
            return make_response('token is invalid', 401, {'Authentication': 'Login required'})

        return f(current_user, *args, **kwargs)
    return decorator
